package wrappers;

import edu.wpi.first.wpilibj.PWMSpeedController;
import edu.wpi.first.wpilibj.hal.FRCNetComm.tResourceType;
import edu.wpi.first.wpilibj.hal.HAL;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

public class MotorController29 extends PWMSpeedController {
	/*
	 * Constructor
	 * 
	 * <p> Use for Vex Motor Controller 29
	 * 
	 * <p> According to SPECIFICATION: MaxPWM: 2ms - Neutral: 1.5ms - Minimum: 1ms
	 */
	public MotorController29(final int channel) {
		super(channel);
		
		setBounds(2d, 1.52, 1.5, 1.48, 1d);
		setPeriodMultiplier(PeriodMultiplier.k1X);
		setSpeed(0d);
		setZeroLatch();
		
		LiveWindow.addActuator("MotorController29", getChannel(), this);
		HAL.report(tResourceType.kResourceType_Controller, getChannel());
	}
}
