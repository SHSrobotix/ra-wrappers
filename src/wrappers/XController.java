package wrappers;
import java.util.HashMap;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;

public class XController {
	//TODO: temporary public?f
	public XboxController controller;
	
	//button range is 1-10
	final int A = 1;
	final int B = 2;
	final int X = 3;
	final int Y = 4;
	final int BUMPERL = 5;
	final int BUMPERR = 6;
	final int BACK = 7;
	final int START = 8;
	

	HashMap<Integer, Boolean> currentFrame = new HashMap<Integer, Boolean>();
	HashMap<Integer, Boolean> lastFrame = new HashMap<Integer, Boolean>();
	
	public XController(XboxController controller) {
		this.controller = controller;
		Init();
	}
	
	public XController(int port) {
		this.controller = new XboxController(port);
		Init();
	}
	
	void Init() {
		for (int i = 1; i <= 10; i++) {
			currentFrame.put(i, controller.getRawButton(i));
		}
	}
	
	//Needs to be called before anything else
	public void Poll() {
		lastFrame = new HashMap<Integer, Boolean>(currentFrame);
		for (int i = 1; i <= 10; i++) {
			currentFrame.put(i, controller.getRawButton(i));
		}
	}
	
	public boolean PressedA() {
		return GetPressed(A);
	}
	
	public boolean PressedX() {
		return GetPressed(X);
	}
	
	public boolean PressedY() {
		return GetPressed(Y);
	}
	
	public boolean PressedB() {
		return GetPressed(B);
	}
	
	public boolean PressedLBumper() {
		return GetPressed(BUMPERL);
	}
	
	public boolean PressedRBumper() {
		return GetPressed(BUMPERR);
	}
	
	public boolean PressedStart() {
		return GetPressed(START);
	}
	
	public boolean PressedBack() {
		return GetPressed(BACK);
	}
	
	public double GetX(Hand hand) {
		return controller.getX(hand);
	}
	
	public double GetY(Hand hand) {
		return controller.getY(hand);
	}
	
	public double GetTriggerAxis(Hand hand) {
		return controller.getTriggerAxis(hand);
	}
	
	public boolean GetAButton() {
		return controller.getAButton();
	}
	
	public boolean GetBButton() {
		return controller.getBButton();
	}
	
	public boolean GetYButton() {
		return controller.getYButton();
	}
	
	public boolean GetXButton() {
		return controller.getXButton();
	}
	
	public boolean GetStartButton() {
		return controller.getStartButton();
	}
	
	public boolean GetModeButton() {
		return controller.getBackButton();
	}
	
	public boolean GetLeftBumper() {
		return controller.getBumper(Hand.kLeft);
	}
	
	public boolean GetRightBumper() {
		return controller.getBumper(Hand.kRight);
	}
	
	boolean GetPressed(int index) {
		if(index < 1 || index > 10) {
			//Valid XBOX buttons 1-14
			throw new IllegalArgumentException();
		}
		
		boolean current = currentFrame.get(index);
		boolean last = lastFrame.get(index);
		//System.out.println(Boolean.toString(current) + "::" + Boolean.toString(last));
		//if last was false and current is true
		if((last == false) && (current == true)) {
			return true;
		} else {
			return false;
		}
	}
}
